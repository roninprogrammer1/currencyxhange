# CurrencyXhange

Build an Currency Exchange app which Currency conversion tool using live exchange rates.

## Use Case 

- Check a list of currency rates against any currency of the user’s choice(base currency).
- Convert any amount from the user’s chosen base currency to a selected currency.
- View currency rates even without an internet connection. Yet, still able to refresh the latest rates by tapping the refresh button.


## API Service 
Use Exchanges Rates API(https://exchangeratesapi.io/) to fetch data.


## Architecture 

This app implements the MVC architectural pattern using a activity receive all the events and user inputs from the View (Controller) and other class working with data as the Model.  



### Built with
* <a href="https://square.github.io/retrofit" target="_blank">Retrofit</a> - Type-safe HTTP client for Android and Java by Square, Inc.
* <a href="https://developer.android.com/guide/navigation" target="_blank">Navigation</a> - A library that can manage complex navigation, transition animation, deep linking, and compile-time checked argument passing between the screens in your app.


### Acknowledgments
* App icon made by Freepik from www.flaticon.com




### License
```
 Copyright 2020 roninprogrammer

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
